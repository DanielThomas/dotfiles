# VMware Fusion #

## vmControl ##

Function for controlling Virtual Machines using vmrun. To use:

- Create aliases using prefixed with vm: `export vm_oracle=~/Documents/Virtual\ Machines.localized/rh6x64-vm000.vmwarevm/rh6x64-vm000.vmx`
- Use that alias to start or stop the machine: `vmControl start oracle`

The function assumes that VMware Fusion is installed in `/Applications/VMware Fusion.app`.
